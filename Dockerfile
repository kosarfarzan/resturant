FROM node
workdir /app
copy . .
RUN npm install
CMD npm start