
const express = require('express');
const path = require('path');

const app = express();

const htmlFilePath = path.join(__dirname,"public", 'index.html');
app.use(express.static("public"))
app.get('/', (req, res) => {
    res.sendFile(htmlFilePath);
});
module.exports = app;