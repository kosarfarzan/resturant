const request = require('supertest');
const app = require('./../app'); // Adjust the path based on your project structure

describe('Express App', () => {

    test('GET / responds with status 200', async () => {
        const response = await request(app).get('/');
        expect(response.status).toBe(200);
    });

    test('GET / responds with HTML content', async () => {
        const response = await request(app).get('/');
        expect(response.headers['content-type']).toMatch(/html/);
    });

    test('GET hve part of html"', async () => {
        const response = await request(app).get('/');
        expect(response.text).toMatch("<!DOCTYPE html>");
    });

    test('Example Test 4', () => {
        expect(true).toBeTruthy();
    });

    test('Example Test 5', () => {
        expect(1 + 1).toBe(2);
    });
});

